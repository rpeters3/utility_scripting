#!/usr/bin/env python

# this hits ths rest api of a specified jenkins installtion and gets a list of all jobs
# and views then displays the count/name of said jobs and views
import json
import subprocess

def main():
  jenkins_url = '' # url of jenkins api endpoint
  cmd = 'curl --silent %s' % jenkins_url

  raw_json = get_output(cmd)
  data = json.loads(raw_json)
  
  jobs = data['jobs']
  job_count = len(jobs)

  views = data['views']
  view_count = len(views)

  print 'JOBS:  %s' % job_count
  print '\t', '\n\t'.join([job['name'] for job in jobs])
  print ''
  print 'VIEWS: %s' % view_count
  print '\t', '\n\t'.join([view['name'] for view in views])

def get_output(cmd):
  process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=None, shell=True)
  return process.communicate()[0].strip()

if __name__ == '__main__':
  main()
