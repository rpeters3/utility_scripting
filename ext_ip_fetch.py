#!/usr/bin/env python

# this is just a quick script to get the external IP address of the local machine

import os
import urllib2
import re
def check_in():
    fqn = os.uname()[1]
    ext_ip = urllib2.urlopen('http://whatismyip.org').read()
    print 'Asset: %s ' % fqn
    m = re.search('.*[^\d]{1}(\d+\.\d+\.\d+\.\d+).*', ext_ip)
    print 'IP: %s' % m.group(1)

if __name__ == '__main__':
    check_in()
