/*
this is a sample hello world script in go
*/

package main

import "fmt"

func main() {
  fmt.Println("hello world")
}
