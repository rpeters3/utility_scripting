#!/usr/bin/env python

# reference code for quickly reading a yaml file into a python dict structure

import re
import sys
import yaml
from pprint import pprint

def main():
  filename = sys.argv[1].strip()

  with open(filename) as infile:
    data = yaml.load(infile)
    perform(data)

def perform(yaml_dict):
  matched_keys = []
  for key in yaml_dict.keys():
    try:
      if re.match('10\.191\.5[23]\.\d+', yaml_dict[key]['ipaddress']):
        matched_keys.append(key)
    except:
        continue

  matched_keys.sort()
  for key in matched_keys:
    print key
    print '\t- %s' % yaml_dict[key]['ipaddress']

if __name__ == '__main__':
  main()
