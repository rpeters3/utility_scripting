#!/usr/bin/env python

# This is just some demo code utilizing python's curses library. The code mimics how
# an rpm gauge might be visually modeled using the library.

import curses
import time
import random

COUNTER_WIDTH = 30
GREEN_TH = .65
YELLOW_TH = .85
RPM_MAX_VAL = 8000
OFFSET = 2

def main():
  curses.wrapper(show_progress)


def show_progress(scr):
  setup()
  win = curses.newwin(10,60,0,2)

  cycle(win, 'RPM', refresh=.003)
  #random_cycle(win, 'RPM', refresh=.02)

  win.refresh()
  time.sleep(1)


def random_cycle(win, unit, refresh=.005):
    for x in range(5):
    #while(True):
      count = random.randint(0, MAX_VAL)
      update(win, count, MAX_VAL, 'RPM', refresh=refresh)


def cycle(win, unit, refresh=.02):
  for x in range(5):
    feed = range(0, 8001, 50)
    #while(True):
    for x in feed:
      update(1, win, x, RPM_MAX_VAL, 'RPM', refresh=refresh)
    for x in feed[::-1]:
      update(1, win, x, RPM_MAX_VAL, 'RPM', refresh=refresh)


def update(row, win, count, max_val, unit, refresh=.1):
  act_c = ratio(count, max_val)
  color = get_color(count, max_val)

  cstring = '=' * act_c
  space = ' ' * (COUNTER_WIDTH - act_c)
  cstring = cstring + space

  green  = cstring[0:int(len(cstring) * GREEN_TH) + 1]
  yellow = cstring[len(green) - 1:int((len(cstring) - 1) * YELLOW_TH)]
  red    = cstring[len(green) + len(yellow) - 1:-1]

  win.addstr(row, 1, '[')
  win.addstr(row, OFFSET, green, curses.color_pair(3))
  win.addstr(row, len(green) + OFFSET, yellow, curses.color_pair(2))
  win.addstr(row, len(green) + len(yellow) + OFFSET, red, curses.color_pair(1))
  win.addstr(row, COUNTER_WIDTH + OFFSET, '] %s: %04d' % (unit, count))
  win.refresh()
  time.sleep(refresh)


def get_color(count, max_val):
  color = curses.color_pair(1) # red default
  if count <= GREEN_TH * max_val:
    color = curses.color_pair(3)
  elif count <= YELLOW_TH * max_val:
    color = curses.color_pair(2)
  return color


def ratio(val, max_val):
  result = (val * COUNTER_WIDTH) / max_val
  return result


def setup():
  curses.start_color()
  curses.use_default_colors()
  curses.init_pair(1, curses.COLOR_RED, -1)
  curses.init_pair(2, curses.COLOR_YELLOW, -1)
  curses.init_pair(3, curses.COLOR_GREEN, -1)


if __name__ == '__main__':
  main()
