#!/usr/bin/env python

# use this script to view a list of available IP addresses when dns is managed by
# infoblox.

import os
import json
from pprint import pprint

ib_api_url = ""
sys_command = "curl -Ss -k -u 'srv_bestinfoblox:RejA*eD7Sw' -X GET -H 'Content-Type: application/json' %s" % ib_api_url
networks = ['10.0.0.0/24/', '10.1.0.0/23/'] # network cidrs to query

def main():
  results = []
  available = []
  print 'Available IP Addresses:'
  for network in networks:
    raw_output = os.popen(sys_command + network).read()
    network_results = json.loads(raw_output)
    network_available = len([instance for instance in network_results if instance['status'] != 'USED'])
    print '\t%s: %s' % (network.rstrip('/'), network_available)
    results.extend(json.loads(raw_output))
    
  #for address in results:
  #  if address['status'] != 'USED':
  #    available.append(address['ip_address'])
  #pprint(available)
  #print ""
  #print "Total: " + str(len(available))

if __name__ == '__main__':
  main()
