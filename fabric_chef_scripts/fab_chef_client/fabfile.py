# this is a fabric/chef utility script designed to upgrade rhel6 chef-client 11
# nodes to chef-client 12. It can also upgrade rhel5 boxes to chef-client 12.
# additionally, it can downgrade rhel5 boxes from chef-client 12 to 11.
# the omnibus updater recipe must be available as well as the included chef-upgrade role
# and the chef-downgrade role

import os
import sys
import time
import base64
from fabric.api import run, sudo, env, warn_only
from pprint import pprint

env.password = base64.decodestring('')
NODE_LIST = 'current_nodes.txt'
chef_rpm_url = ''
chef_12_rh6_rpm = ''
chef_12_rh5_rpm = ''
chef_11_rh5_rpm = ''

# read in nodes from a text file
with open(NODE_LIST) as infile:
  env.hosts = [line.rstrip('\n') for line in infile if line != '\n']

# verify env.hosts list
pprint(env.hosts)
if raw_input('Press Y to continue or N to cancel:\n').lower().startswith('n'):
    sys.exit('Cancelling fabric run!')

def version():
  run('chef-client -v')

# upgrade a chef-client node to the version specified in the chef-upgrade role
def upgrade():
  with warn_only():
    split = '=' * 50
    start = '%s %s %s' % (split, env.host, split)
    end = '=' * len(start)
    print '\n' + start
    sudo("chef-client -o 'role[chef-upgrade]'", shell=False)
    print end + '\n'

# upgrade a rhel5 node to chef-client 12
def upgrade5():
  with warn_only():
    run("wget %s" % os.path.join(chef_rpm_url, chef_12_rh5_rpm))
    sudo("rpm -Uvh --oldpackage %s" % chef_12_rh5_rpm)
    run("rm %s" % chef_12_rh5_rpm)

# downgrade a rhel5 node to chef-client 11
def downgrade5():
  with warn_only():
    run("wget %s" % os.path.join(chef_rpm_url, chef_11_rh5_rpm))
    run("rpm -Uvh --oldpackage %s" % chef_11_rh5_rpm)
    run("rm %s" % chef_11_rh5_rpm)
    # optionally, check the version after the downgrade
#    time.sleep(2)
#    run("chef-client -v")

# downgrade a rhel6 node to a version of chef-client specified in the chef-downgrade role
def downgrade():
  with warn_only():
    divider = '=' * 50
    header = '%s %s %s' % (divider, env.host, divider)
    end = '=' * len(header)
    print '\n' + header
    sudo("chef-client -o 'role[chef-downgrade]'", shell=False)
# optionally, check the version after the downgrade
#    time.sleep(2)
#    run("chef-client -v")

# list the contents of the omnibus_updater cookbooks cache
def cache():
  run("ls -lh /var/chef/cache/omnibus_updater")

# get the os version of the rhel/centos node
def centosv():
  run("cat /etc/redhat-release | grep -o '[0-9]\.[0-9]'")

# get the system architecture of the node
def arch():
  run("getconf LONG_BIT")

# get the chef client version of the node
def version():
  with warn_only():
    run('chef-client -v', shell=False)
