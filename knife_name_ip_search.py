#!/usr/bin/env python

# Perform a knife search and return only the name and IP address of results

import sys
import os
import json
import subprocess

knife_dir = ''

def main():
  chef_org = sys.argv[1].upper()
  name = sys.argv[2]
  path = os.path.join(knife_dir, chef_org)

  cmd = 'cd %s && knife search node "name:*%s*" -a ipaddress --format json' % (path, name)
  raw_out = get_output(cmd)
  tmp = json.loads(raw_out)
  res = tmp['rows']
  res.sort()

  for res in res:
    print res.keys()[0]
    print res[res.keys()[0]]['ipaddress']
    print ''


def get_output(cmd):
  process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=None, shell=True)
  return process.communicate()[0].strip()


if __name__ == '__main__':
  main()