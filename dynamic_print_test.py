#!/usr/bin/env python

# this script is a reference to 're-printing' the same line within python
from sys import stdout
from time import sleep
import os
import random

lst = ['atom-something', 'best-better', 'meta-stuff', 'bps-best-meta-junk']

for repo in lst:
    size = random.randint(0,9)
    stdout.write("\r%s: %s" % (repo, size))
    stdout.flush()
    if size > 6:
        print '\n\ngit pull...\nsomething something\nsomething something\ndone.'
    sleep(1)
stdout.write("\n")
